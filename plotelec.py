#!/usr/bin/python3.4
# Arthur Hagopian <arthur.hagopian@umontpellier.fr>, version 26/11/2021


# THIS SCRIPT ALLOWS TO PLOT THE RESULTS OF GCDFT CALCULATIONS AND TO COMPUTE SOME PROPERTIES
# 1 INPUT IS REQUIRED (GENERATED FROM outelecAll.py SCRIPT) : outelecAll.dat
# DATA COMPUTED : 
# OUTPUT FILE GENERATED : plotelec.dat


### MODULES ###
from os import listdir, chdir
from os.path import isfile, isdir, join
import re
import os
import numpy as np
import matplotlib.pyplot as plt

### PATHWAYS ###
mypath = "./"

### PLOT PARAMETERS ###
plt.rcParams.update({'font.size': 22})
fig, ax = plt.subplots(1, figsize=(7, 5))


### GET DATA IN outelecAll.dat ###
filename = join(mypath, "outelecAll.dat")
if(isfile(filename)):
    data = np.loadtxt(filename)
    electron_number = data[:, 0]
    work_function = data[:, 1]
    free_energy = data[:, 2]
    grand_potential = data[:, 3]
else:
    print("ERROR: outelecAll.dat FILE MISSING. PLEASE RUN outelecAll.py SCRIPT.")

### COMPUTE CAPACITANCES AND VACUUM FRACTION ###
print("\n##### GCDFT PLOT #####\n")
# From Charge vs Potential curve
fit = np.polyfit(work_function, electron_number, 1) # 1st order fit
capacitance_1 = -fit[0]
# From Grand potential vs Potential curve
fit = np.polyfit(work_function, grand_potential, 2) # 2nd order fit
capacitance_2 = -fit[0] * 2
# Vacuum fraction
vacuum_fraction = capacitance_2 / capacitance_1
# Print everything
print("Capacitance from Charge vs Potential curve [e/V] : %12.10f \n" % (capacitance_1))
print("Capacitance from Grand Potential vs Potential curve [e/V] : %12.10f \n" % (capacitance_2))
print("Vacuum fraction : %12.10f \n" % (vacuum_fraction))

### GET INFORMATION FROM USER ###
plot_choice = int(input(("What do you want to plot ? \n1. Charge vs Potential \n2. Free energy vs Potential \n3. Grand potential vs Potential \n")))

### PLOT ###
# Charge vs Potential
if plot_choice == 1:
    ax.scatter(work_function, electron_number, marker="o", s=150, edgecolors="black", zorder=3)
    ax.plot(work_function, electron_number, color="black", linewidth=2, linestyle="-", zorder=2)
    ax.set(xlabel="$\Phi [V]$", ylabel="$\Delta N_e [e]$")
    ax.grid(zorder=1)
# Free energy vs Potential
elif plot_choice == 2:
    ax.scatter(work_function, free_energy, marker="o", s=150, edgecolors="black", zorder=3)
    ax.plot(work_function, free_energy, color="black", linewidth=2, linestyle="-", zorder=2)
    ax.set(xlabel="$\Phi [V]$", ylabel="$F [eV]$")
    ax.grid(zorder=1)
# Grand potential vs Potential
elif plot_choice == 3:
    ax.scatter(work_function, grand_potential, marker="o", s=150, edgecolors="black", zorder=3)
    ax.plot(work_function, grand_potential, color="black", linewidth=2, linestyle="-", zorder=2)
    ax.set(xlabel="$\Phi [V]$", ylabel="$\Omega [eV]$")
    ax.grid(zorder=1)
plt.savefig("figure.pdf")
plt.show()

