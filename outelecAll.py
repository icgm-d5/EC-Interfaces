#!/usr/bin/python3.4
# Arthur Hagopian <arthur.hagopian@umontpellier.fr>, version 26/11/2021


# THIS SCRIPT ALLOWS THE TREATMENT OF GCDFT CALCULATIONS
# 1 PYTHON SCRIPT IS REQUIRED : outelecOne.py
# DATA COMPUTED : GRAND POTENTIAL, WORK FUNCTION AND ELECTRON NUMBER VARIATION (RELATIVE TO PZC)
# OUTPUT FILE GENERATED : outelecAll.dat


### MODULES ###
from os import listdir, chdir
from os.path import isfile, isdir, join
import re
import os
import subprocess

### PATHWAYS ###
mypath = "./"
onlydir = sorted([f for f in listdir(mypath) if (isdir(join(mypath, f)))])

### USE DEVNULL ###
devnull = open(os.devnull, 'w')

### EXECUTE outelecOne.py IN EVERY FOLDERS ###
print("\n##### GCDFT CALCULATIONS #####\n")
data = {} # Dictionary with : key = electron_number, value = [work_function, grand_potential]
for dir in onlydir:
    chdir(dir)
    subprocess.call("outelecOne.py", stdout = devnull)
    filename = ("outelecOne.dat")
    if(isfile(filename)):
        with open(filename) as f:
            content = f.readlines()
            for line in content:
                elts = line.split()
                data[float(elts[0])] = [float(elts[1]), float(elts[2]), float(elts[3])]
        print(str(dir) + " : done")
    chdir("..")

### PRINT EVERYTHING IN OUTPUT FILE ###
file_out = open(mypath + "outelecAll.dat", "w")
for key in sorted(data):
    file_out.write("%12.10f" % (key) + "    " + "%12.10f" % (data[key][0]) + "    " + "%12.10f" % (data[key][1]) + "    " + "%12.10f" % (data[key][2]) + "\n")
file_out.close()

