#!/usr/bin/python3.4
# Arthur Hagopian <arthur.hagopian@umontpellier.fr>, version 26/11/2021

# THIS SCRIPT ALLOWS TO GENERATE THE INPUT FOLDERS FOR GCDFT CALCULATIONS
# 5 INPUT FILES ARE REQUIRED : INCAR, POSCAR, POTCAR, KPOINTS, job


### MODULES ###
from os.path import isfile, join
from re import match
import os
import shutil
import sys

### PATHWAYS ###
mypath = "./"

### CHECK FOR ALL INPUTS ###
inputs = ["INCAR", "POSCAR", "POTCAR", "KPOINTS", "job"]
input_missing = False
for elt in inputs:
    inp = join(mypath, elt)
    if(isfile(elt)):
        pass
    else:
        print("ERROR : " + str(elt) + " MISSING")
        input_missing = True
if input_missing == True:
    sys.exit()

### GET INFORMATION FROM USER ###
# Prefix for folders
prefix = input("Prefix for folders : ")
# Maximum number of electron to add : integer or fractional
electron_added = float(input("Maximum number of added electron : "))
# Maximum number of electron to remove : integer or fractional
electron_removed = float(input("Maximum number of removed electron : "))
# Step to add/remove electron : integer or fractional
step = float(input("Step number of added/removed electron : "))

### COMPUTE ELECTRON NUMBER ON NEUTRAL SYSTEM ###
# Get the number of each element in POSCAR
filename = join(mypath, "POSCAR")
with open(filename) as f:
    content = f.readlines()
    atom_numbers = content[6].split()
    atom_numbers = [int(x) for x in atom_numbers]
# Get electron number for each element in POTCAR
filename = join(mypath, "POTCAR")
new_element = False
electron_numbers = []
with open(filename) as f:
    content = f.readlines()
    for line in content:
        elts = line.split()
        if new_element == True:
            electron_numbers.append(float(elts[0]))
            new_element = False
        if(len(elts) > 2 and elts[0] == "PAW_PBE"):
            new_element = True
# Compute electron number on neutral cell (PZC)
i = 0
electron_number_pzc = 0
while i < len(atom_numbers):
    electron_number_pzc += atom_numbers[i] * electron_numbers[i]
    i += 1
# Write NELECT of neutral system in INCAR and ensure CHGCAR and LOCPOT are written
f = open("INCAR", "r")
content = f.readlines()
f.close()
f = open("INCAR", "w")
for line in content:
    matching_nelect = match(".*NELECT.*",line)
    matching_lvtot = match(".*LVTOT.*",line)
    matching_lvhar = match(".*LVHAR.*",line)
    matching_lcharg = match(".*LCHARG.*",line)
    if matching_nelect is None and matching_lvtot is None and matching_lvhar is None and matching_lcharg is None:
        f.write(line)
f.write("  LVTOT = .TRUE.\n")
f.write("  LVHAR = .TRUE.\n")
f.write("  LCHARG = .TRUE.\n")
f.write("  NELECT = " + str(electron_number_pzc) + "\n")
f.close()

### CREATE INPUT FOLDERS ###
# Creat folder for neutral cell
folder_neutral_cell = prefix + "_" + "%07.3f" % (electron_number_pzc) + "_neutral"
os.mkdir(folder_neutral_cell)
for elt in inputs:
    shutil.move(elt, folder_neutral_cell)
# Loop to add electrons
i = step
while i < electron_added:
    electron_number = electron_number_pzc + i
    folder = prefix + "_" + "%07.3f" % (electron_number)
    shutil.copytree(folder_neutral_cell, folder)
    filename = join(folder, "INCAR")
    f = open(filename, "r")
    content = f.readlines()
    f.close()
    f = open(filename, "w")
    for line in content:
        matching = match(".*NELECT.*",line)
        if matching is None:
            f.write(line)
    f.write("  NELECT = " + str(electron_number))
    f.close
    i += step
# Loop to remove electrons
i = step
while i < electron_removed:
    electron_number = electron_number_pzc - i
    folder = prefix + "_" + "%07.3f" % (electron_number)
    shutil.copytree(folder_neutral_cell, folder)
    filename = join(folder, "INCAR")
    f = open(filename, "r")
    content = f.readlines()
    f.close()
    f = open(filename, "w")
    for line in content:
        matching = match(".*NELECT.*",line)
        if matching is None:
            f.write(line)
    f.write("  NELECT = " + str(electron_number))
    f.close
    i += step

