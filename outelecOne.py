#!/usr/bin/python3.4
# Arthur Hagopian <arthur.hagopian@umontpellier.fr>, version 26/11/2021


# THIS SCRIPT ALLOWS THE TREATMENT OF GCDFT CALCULATION
# DATA COMPUTED : ELECTRON NUMBER (RELATIVELY TO PZC), WORK_FUNCTION AND GRAND POTENTIAL
# OUTPUT FILE GENERATED : outelecOne.dat


### MODULES ###
from os.path import isfile, join
from re import match
import sys

### PATHWAYS ###
mypath = "./"

### CHECK CONVERGENCE OF THE CALCULATIONIN THE OUTCAR ###
convergence = False
filename = join(mypath, "OUTCAR")
if(isfile(filename)):
    with open(filename) as f:
        content = f.readlines()
        for line in content:
            matching = match(".*Voluntary context switches.*", line)
            if not(matching is None):
                convergence = True
if convergence != True:
    print("ERROR: Calculation not converged.")
    sys.exit()

### GET FREE ENERGY IN OSZICAR ###
filename = join(mypath, "OSZICAR")
if(isfile(filename)):
    with open(filename) as f:
        content = f.readlines()
        matching = match("SOL:", content[-1])
        # Without implicit solvent
        if(matching is None):
            matching = match(".*E0= ([0-9E\-\+.]+) ",content[-1])
            if not (matching is None):
                free_energy = float(matching.group(1))
        # With implicit solvent
        else:
            matching = match(".*E0= ([0-9E\-\+.]+) ",content[-2])
            if not (matching is None):
                free_energy = float(matching.group(1))

### GET ELECTRON NUMBER AND FERMI ENERGY IN OUTCAR ###
filename = join(mypath, "OUTCAR")
if(isfile(filename)):
    with open(filename) as f:
        content = f.readlines()
        for line in content:
            matching_nelect = match(".*NELECT.*", line)
            if not(matching_nelect is None):
                elts = line.split()
                electron_number = float(elts[2])
            if(line[:8] == " E-fermi"):
                matching_fermi = match(".*E-fermi :[ ]+([0-9E\-\+.]+) ",line)
                if not(matching_fermi is None):
                    fermi_energy = float(matching_fermi.group(1))

### GET ELECTRON NUMBER OF NEUTRAL CELL (PZC) ###
# Get the number of each element in POSCAR
filename = join(mypath, "POSCAR")
with open(filename) as f:
    content = f.readlines()
    atom_numbers = content[6].split()
    atom_numbers = [int(x) for x in atom_numbers]
    atom_tot = int(sum(atom_numbers))
    z_line = content[4].split()
    z_cell = float(z_line[2])
# Get electron number for each element in POTCAR
filename = join(mypath, "POTCAR")
new_element = False
electron_numbers = []
with open(filename) as f:
    content = f.readlines()
    for line in content:
        elts = line.split()
        if new_element == True:
            electron_numbers.append(float(elts[0]))
            new_element = False
        if(len(elts) > 2 and elts[0] == "PAW_PBE"):
            new_element = True
# Compute electron number on neutral cell (PZC)
i = 0
electron_number_pzc = 0
while i < len(atom_numbers):
    electron_number_pzc += atom_numbers[i] * electron_numbers[i]
    i += 1

### COMPUTE THE ELECTRON NUMBER VARIATION (RELATIVE TO PZC) ###
electron_number_variation = electron_number - electron_number_pzc

### COMPUTE GRAND POTENTIAL ###
grand_potential = free_energy - electron_number_variation * fermi_energy

### COMPUTE WORK FUNCTION ###
# 1 - Determine the center of the vacuum
#  1.a - Localise the vacuum region : determine the charge density minimum from CHGCAR integration
#  1.b - Get a precise definition of the vacuum center : determine the positions of the atoms the closet of the charge density minimum under and above, and take the average value
# 2 - Get the potential value to this point (reference potential)
# 3 - Compute work function

### 1 - DETERMINE THE CENTER OF THE VACUUM ###
### 1.a - LOCALISE THE VACUUM REGION : DETERMINE THE CHARGE DENSITY MINIMUM FROM CHGCAR INTEGRATION ###
filename = join(mypath, "CHGCAR")
if(isfile(filename)):
    with open(filename) as f:
        content = f.readlines()
# Get the a1, a2, a3 vectors
scale = float(content[1].split()[0])
a1_vec = [scale*float(val) for val  in  content[2].split()]
a2_vec = [scale*float(val) for val  in  content[3].split()]
a3_vec = [scale*float(val) for val  in  content[4].split()]
# Check if z-coordinate is orthogonal to slab surface plane
if a1_vec[2] != 0.0 or a2_vec[2] != 0.0 or a3_vec[0] != 0.0 or a3_vec[1] != 0.0:
    print("ERROR: z-coordinate is not orthogonal to slab surface plane.")
    sys.exit()
# Get z_tot of the cell
z_tot = a3_vec[2]
# Give the z_center
z_center = 0
# Get the nX, nY, nZ values
nX = int(content[9 + atom_tot].split()[0])
nY = int(content[9 + atom_tot].split()[1])
nZ = int(content[9 + atom_tot].split()[2])
# Get the number of points
number_points = nX * nY * nZ
# Get all data points
points = []
i = 9 + atom_tot
while(len(points) < number_points):
    i += 1
    points += [float(val) for val  in  content[i].split()]
# Compute x-y average values
projected_values = []
for iz in range(nZ):
    area_average = 0.0
    for i in range(iz * nX * nY, (iz + 1) * nX * nY):
        area_average += points[i]
    area_average /= (nX * nY)
    projected_values.append(area_average)
# Print z values (in angstrom) and projected values in .dat file
file_out = open(mypath + "chgcar_xy_avg.dat", "w")
for iz in range(nZ):
    file_out.write(str(iz * z_tot / nZ) + '\t' + str(projected_values[iz]) + '\n')
file_out.close()
# Get the z value associated with the charge density minimum
z = []
charge_density = []
filename = join(mypath, "chgcar_xy_avg.dat")
if(isfile(filename)):
    with open(filename) as f:
        content = f.readlines()
        for line in content:
            elts = line.split()
            z.append(elts[0])
            charge_density.append(elts[1])
        min_charge_density = min(charge_density)
z_min_charge_density = float(z[charge_density.index(min_charge_density)])

### 1.b - DETERMINE THE POSITIONS OF THE ATOMS THE CLOSET OF THE CHARGE DENSITY MINIMUM UNDER AND ABOVE ###
# Get z values of each atom in POSCAR
z_values = []
filename = join(mypath, "POSCAR")
if(isfile(filename)):
    with open(filename) as f:
        content = f.readlines()
        start = False
        stop = False
        for line in content:
            if(line == 'Direct\n') or (line == 'direct\n') or (line == 'Cartesian\n') or (line == 'cartesian\n'):
                start = True
            elif(line==" \n"):
                stop = True
            else:
                elts = line.split()
                if(len(elts) > 2 and start == True and stop != True):
                    z_values.append(float(elts[2]))
# Check if z are given in fractional coordinates
fractional_z = True
for z in z_values:
    if z > 1:
        fractional_z = False
if fractional_z == True:
    z_values = [z * z_cell for z in z_values]
# Determine the positions of the atoms the closet of the charge density minimum
z_distance_above = z_distance_under = 1000
for z in z_values:
    if z > z_min_charge_density:
        z_distance = abs(z_min_charge_density - z)
        if z_distance < z_distance_above:
            z_distance_above = z_distance
            z_max = z
    if z < z_min_charge_density:
        z_distance = abs(z_min_charge_density - z)
        if z_distance < z_distance_under:
            z_distance_under = z_distance
            z_min = z
# Check if no z_max
try:
    z_max + 1
except:
    for z in z_values:
        z_corrected = z + z_cell
        z_distance = abs(z_min_charge_density - z_corrected)
        if z_distance < z_distance_above:
            z_distance_above = z_distance
            z_max = z_corrected
# Check if no z_min
try:
    z_min + 1
except:
    for z in z_values:
        z_corrected = z - z_cell
        z_distance = abs(z_min_charge_density - z_corrected)
        if z_distance < z_distance_under:
            z_distance_under = z_distance
            z_min = z_corrected

# Computed vacuum center and check that it is contained in the cell
vacuum_center = (z_max + z_min) / 2
if 0 <= vacuum_center <= z_cell:
    pass
elif vacuum_center > z_cell:
    vacuum_center = vacuum_center - z_cell
elif vacuum_center < 0:
    vacuum_center = vacuum_center + z_cell

### 2 - COMPUTE THE REFERENCE POTENTIAL FROM LOCPOT INTEGRATION ###
# Integrate LOCPOT
filename = join(mypath, "LOCPOT")
if(isfile(filename)):
    with open(filename) as f:
        content = f.readlines()
# Get all data points
points = []
i = 9 + atom_tot
while(len(points) < number_points):
    i += 1
    points += [float(val) for val  in  content[i].split()]
# Compute x-y average values
projected_values = []
for iz in range(nZ):
    area_average = 0.0
    for i in range(iz * nX * nY, (iz + 1) * nX * nY):
        area_average += points[i]
    area_average /= (nX * nY)
    projected_values.append(area_average)
# Print z values (in angstrom) and projected values in .dat file
file_out = open(mypath + "locpot_xy_avg.dat", "w")
for iz in range(nZ):
    file_out.write(str(iz * z_tot / nZ) + "\t" + str(projected_values[iz]) + '\n')
file_out.close()
# Determine the reference potential from LOCPOT integration
z = []
potential = []
filename = join(mypath, "locpot_xy_avg.dat")
if(isfile(filename)):
    with open(filename) as f:
        content = f.readlines()
        for line in content:
            elts = line.split()
            z_values.append(float(elts[0]))
            potential.append(float(elts[1]))
z_index_win = 0
for z_index in range(nZ):
    z_now = z_index * z_cell / nZ
    z_win = z_index_win * z_cell / nZ
    if abs(z_now - vacuum_center) < abs(z_win - vacuum_center):
        z_index_win = z_index
reference_potential = float(potential[z_index_win])

### 3 - COMPUTE WORK FUNCTION ###
work_function = reference_potential - fermi_energy

### PRINT EVERYTHING ON THE USER'S SCREEN ###
print("\n##### GCDFT CALCULATION #####\n")
print("Electron number (relatively to PZC) (e) : %12.10f \n" % (electron_number_variation))
print("Work function (V) : %12.10f \n" % (work_function))
print("Free energy (eV) : %12.10f \n" % (free_energy))
print("Grand potential (eV) : %12.10f \n" % (grand_potential))

### PRINT EVERYTHING IN OUTPUT FILE ###
file_out = open(mypath + "outelecOne.dat", "w")
file_out.write("%12.10f" % (electron_number_variation) + "    " + "%12.10f" % (work_function) + "    " + "%12.10f" % (free_energy) + "    " + "%12.10f" % (grand_potential))
file_out.close()



